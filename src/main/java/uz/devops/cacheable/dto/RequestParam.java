package uz.devops.cacheable.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class RequestParam implements Serializable {
    private String username;
    private String password;
}
