package uz.devops.cacheable.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO<T> {
    private String message;
    private Boolean success;
    private T data = null;

    public ResponseDTO(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }
}
