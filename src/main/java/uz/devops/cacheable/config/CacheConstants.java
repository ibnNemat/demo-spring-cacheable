package uz.devops.cacheable.config;

public class CacheConstants {
    public static final String EHCACHE_MANAGER_BEAN_NAME = "EHCACHE_MANAGER_BEAN_NAME";
    public static final String EHCACHE_BEAN_NAME = "EHCACHE_BEAN_NAME";
    public static final String CAFFEINE_BEAN_NAME = "CAFFEINE_BEAN_NAME";
    public static final String EXCEPTION_MESSAGE = "TOO MANY ATTEMPTS: The process with request key: %s has not been completed yet!";
}
