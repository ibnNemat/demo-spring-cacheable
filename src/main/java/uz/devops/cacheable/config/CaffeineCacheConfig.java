package uz.devops.cacheable.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

import static uz.devops.cacheable.config.CacheConstants.CAFFEINE_BEAN_NAME;

@EnableCaching
@Configuration
public class CaffeineCacheConfig {
    @Bean
    public CacheManager caffeineCacheManager() {
        CaffeineCacheManager cacheManager = new CaffeineCacheManager(CAFFEINE_BEAN_NAME);
        cacheManager.setCaffeine(
                Caffeine.newBuilder()
                        .expireAfterWrite(15, TimeUnit.SECONDS)
                        .maximumSize(1000)
                        .weakKeys()
                        .recordStats()
        );

        return cacheManager;
    }

//    @Bean(name = EHCACHE_BEAN_NAME)
//    @Primary
//    public Cache<RequestParam, UserDTO> cache() {
//
//    }
}
