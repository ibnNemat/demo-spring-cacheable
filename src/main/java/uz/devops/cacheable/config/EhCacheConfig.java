package uz.devops.cacheable.config;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import uz.devops.cacheable.dto.RequestParam;
import uz.devops.cacheable.dto.UserDTO;

import java.io.File;
import java.time.Duration;

import static uz.devops.cacheable.config.CacheConstants.*;

@EnableCaching
@Configuration
public class EhCacheConfig {
    @Bean(name = EHCACHE_MANAGER_BEAN_NAME)
    public CacheManager cacheManager() {
        return CacheManagerBuilder.newCacheManagerBuilder()
//                .with(CacheManagerBuilder.persistence(new File("java.io.tmpdir/ehcache-disk-store")))
                .build(true);
    }

    @Primary
    @Bean(name = EHCACHE_BEAN_NAME)
    public Cache<RequestParam, UserDTO> cache() {
        return cacheManager().createCache(
                EHCACHE_BEAN_NAME,
                CacheConfigurationBuilder.newCacheConfigurationBuilder(
                        RequestParam.class,
                        UserDTO.class,
                        ResourcePoolsBuilder
                                .heap(1000L)
                                .offheap(30, MemoryUnit.MB)
                ).withExpiry(
                        ExpiryPolicyBuilder
                                .expiry()
                                .create(Duration.ofSeconds(15))
                                .build()
                )
        );
    }
}
