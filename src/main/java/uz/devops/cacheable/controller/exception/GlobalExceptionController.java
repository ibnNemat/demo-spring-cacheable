package uz.devops.cacheable.controller.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import uz.devops.cacheable.dto.ResponseDTO;
import uz.devops.cacheable.dto.UserDTO;
import uz.devops.cacheable.exception.IncompleteProcessException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionController {
    @ExceptionHandler(IncompleteProcessException.class)
    public ResponseEntity<ResponseDTO<UserDTO>> handleTooManyRequestException(IncompleteProcessException ex) {
        log.debug("Started exception handler TooManyRequests. Message: {}", ex.getMessage());
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).body(new ResponseDTO<>(ex.getMessage(), false));
    }
}
