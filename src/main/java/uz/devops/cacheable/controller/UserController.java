package uz.devops.cacheable.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.devops.cacheable.dto.ResponseDTO;
import uz.devops.cacheable.dto.UserDTO;
import uz.devops.cacheable.service.UserService;


@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    @PostMapping
    public ResponseEntity<ResponseDTO<UserDTO>> test(@RequestBody UserDTO userDTO) {
        var result = userService.checkProcess(userDTO);
        return ResponseEntity.ok(result);
    }
}
