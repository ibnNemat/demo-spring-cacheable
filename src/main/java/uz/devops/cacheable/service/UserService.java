package uz.devops.cacheable.service;

import uz.devops.cacheable.dto.ResponseDTO;
import uz.devops.cacheable.dto.UserDTO;

public interface UserService {
    void save(UserDTO userDTO);
    ResponseDTO<UserDTO> checkProcess(UserDTO userDTO);
}
