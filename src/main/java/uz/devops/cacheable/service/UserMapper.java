package uz.devops.cacheable.service;

import org.mapstruct.Mapper;
import uz.devops.cacheable.dto.UserDTO;
import uz.devops.cacheable.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDTO toDTO(User user);
    User toEntity(UserDTO userDTO);
}
