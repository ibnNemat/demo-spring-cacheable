package uz.devops.cacheable.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import uz.devops.cacheable.dto.ResponseDTO;
import uz.devops.cacheable.dto.UserDTO;
import uz.devops.cacheable.repo.UserRepository;
import uz.devops.cacheable.service.UserMapper;
import uz.devops.cacheable.service.UserService;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    @Override
    public void save(UserDTO userDTO) {
        log.debug("Request to create user: {}", userDTO);
        var user = userRepository.save(userMapper.toEntity(userDTO));
        log.debug("Result saved user: {}", user);
    }

    @Override
    public ResponseDTO<UserDTO> checkProcess(UserDTO userDTO) {
        log.debug("Started check process");

        return ResponseDTO.<UserDTO>builder()
                .success(true)
                .message("OK")
                .build();
    }
}
