package uz.devops.cacheable.exception;

public class IncompleteProcessException extends RuntimeException {
    public IncompleteProcessException(String message) {
        super(message);
    }
}
