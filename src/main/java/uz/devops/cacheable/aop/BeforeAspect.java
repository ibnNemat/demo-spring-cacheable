package uz.devops.cacheable.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.ehcache.Cache;
import org.springframework.stereotype.Component;
import uz.devops.cacheable.dto.RequestParam;
import uz.devops.cacheable.dto.UserDTO;
import uz.devops.cacheable.exception.IncompleteProcessException;

import static uz.devops.cacheable.config.CacheConstants.EXCEPTION_MESSAGE;

@Aspect
@Component
@Slf4j
@AllArgsConstructor
public class BeforeAspect {
    private final ObjectMapper objectMapper;
    private final Cache<RequestParam, UserDTO> usersCache;
    @Pointcut("within(@org.springframework.stereotype.Service *)")
    private void withinServiceAnnotation() {}

    @Pointcut("execution(public uz.devops.cacheable.dto.ResponseDTO<uz.devops.cacheable.dto.UserDTO> checkProcess(uz.devops.cacheable.dto.UserDTO))")
    private void executeBeforeCheckProcess() {}

    @SneakyThrows
    @Before("withinServiceAnnotation() && executeBeforeCheckProcess()")
    public void beforeCheckProcess(JoinPoint joinPoint) {
        log.debug("Started aspect beforeCheckProcess");
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        var valueAsString = objectMapper.writeValueAsString(joinPoint.getArgs()[0]);
        UserDTO userDTO = objectMapper.readValue(valueAsString, UserDTO.class);

        var requestParam = new RequestParam(userDTO.getUsername(), userDTO.getPassword());
        if (usersCache.containsKey(requestParam)) {
            var exMessage = String.format(EXCEPTION_MESSAGE, requestParam);

            log.warn(exMessage);
            throw new IncompleteProcessException(exMessage);
        }

        usersCache.put(requestParam, userDTO);
        log.debug("Created new cache with data: {}", userDTO);
    }
}
